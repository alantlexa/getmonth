#include <iostream>
#include <time.h>

int getMonth()
{
    struct tm newtime;
    time_t now = time(0);
    localtime_s(&newtime, &now);
    int Month = newtime.tm_mon;
    return Month;
}


int main()
{
    const int size = 5;
    int array[size][size];
    
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }

    int sum = 0;
    int index = getMonth() % size;
    for (int i = 0; i < size; i++)
    {
        sum += array[i][index];
    }
    std::cout << sum;
    std::cout << "\n";

}
